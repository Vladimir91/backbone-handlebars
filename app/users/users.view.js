define(function (require) {
    var Backbone = require('backbone');
    var _ = require('underscore');
    var Handlebars = require('handlebars');


    var UserView = require('user/user.view');

    var html = require('text!./users.template.html');
    var template = Handlebars.compile(html);

    var userList = [
        {id: 1, name: 'Jules'},
        {id: 2, name: 'Vincent'},
        {id: 3, name: 'Marsellus'}
    ];

    return Backbone.View.extend({
        tagName: 'ul',
        id: 'users-list',
        initialize: function () {
            $('body').html(this.el);
            this.subViews = _.map(userList, function (user) {
                return new UserView({model: new Backbone.Model({id: user.id, name: user.name})});
            });

            this.render();
        },
        render: function () {
            _.each(this.subViews, function (view) {
                this.$el.append(view.el);
            }, this);
            this.$el.append(template(this));
        },

        close: function () {
            _.each(this.subViews, function (view) {
                view.remove();
            });
            this.remove();
        }
    });

});