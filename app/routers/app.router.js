define(function (require) {
    var Backbone = require('backbone');
    var HomeView = require('home/home.view');
    var UsersView = require('users/users.view');
    var UserView = require('user/user.view');
    var HandleBarsStuffView = require('stuff/stuff.view')

    return Backbone.Router.extend({
        routes: {
            '': 'home',
            'home': 'home',
            'users': 'users',
            //'users/:id': 'user',
            'stuff': 'stuff'
        },
        home: function () {
            this.loadView(new HomeView());
        },
        users: function () {
            this.loadView(new UsersView());
        },
        user: function () {
            this.loadView(new UserView());
        },
        stuff: function () {
            this.loadView(new HandleBarsStuffView());
        },
        loadView: function (view) {
            this.view && (this.view.close ? this.view.close() : this.view.remove());
            this.view = view;
        }
    });

});