define(function (require) {
    var $ = require('jquery');
    var MainRouter = require('routers/app.router');
    var Backbone = require('backbone');

    $(function () {
        new MainRouter();
        Backbone.history.start();
    });
});