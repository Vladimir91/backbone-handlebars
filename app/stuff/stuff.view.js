define(function (require) {
    var Backbone = require('backbone');
    var _ = require('underscore');
    var Handlebars = require('handlebars');

    var html = require('text!./stuff.template.html');
    var template = Handlebars.compile(html);

    Handlebars.registerHelper('list', function (items, options) {
        var out = "<ul>";

        for (var i = 0; i < items.length; i++) {
            out = out + "<li>" + options.fn(items[i]) + "</li>";
        }

        return out + "</ul>";
    });

    Handlebars.registerHelper('ifIsEqual', function (v1, v2, options) {
        if (v1 === v2) {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });

    Handlebars.registerHelper('ifCond', function (v1, operator, v2, options) {
        switch (operator) {
            case '==':
                return (v1 == v2) ? options.fn(this) : options.inverse(this);
            case '===':
                return (v1 === v2) ? options.fn(this) : options.inverse(this);
            case '!=':
                return (v1 != v2) ? options.fn(this) : options.inverse(this);
            case '!==':
                return (v1 !== v2) ? options.fn(this) : options.inverse(this);
            default:
                return options.inverse(this);

        }
    });

    Handlebars.registerHelper('breadcrumbs', function () {
        var options = arguments[arguments.length - 1];

        var out = "<div>";
        for (var i = 0; i < arguments.length - 2; i++) {
            out += arguments[i] + " > ";
        }
        out += arguments[arguments.length - 2];

        return out + "</div>";
    });

    return Backbone.View.extend({
        tagName: 'div',
        id: 'stuff-list',
        initialize: function () {
            $('body').html(this.el);
            this.userList = [
                {
                    firstName: "Test1FirstName",
                    lastName: "Test1LaststName",
                    details: {cnp: 12345, address: 'dummy test1'}
                },
                {
                    firstName: "Test2FirstName",
                    lastName: "Test2LaststName",
                    details: {cnp: 5645645, address: 'dummy address test2'}
                },
                {
                    firstName: "Test3FirstName",
                    lastName: "Test3LaststName",
                    details: {cnp: 24234234, address: 'dummy address test3'}
                }
            ];

            this.isActive = true;
            this.isInactive = false;

            this.render();
        },
        render: function () {
            this.$el.html(template(this));
        }
    });

});