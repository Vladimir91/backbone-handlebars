define(function (require) {
    var Backbone = require('backbone');
    var Handlebars = require('handlebars');

    var html = require('text!./home.template.html');
    var template = Handlebars.compile(html);

    return Backbone.View.extend({
        tagName: 'div',
        id: 'home-view',

        initialize: function () {
            $('body').html(this.el);
            this.render();
        },

        render: function () {
            this.$el.html(template());
        }
    });
});
