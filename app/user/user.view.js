define(function (require) {
    var Backbone = require('backbone');
    var Handlebars = require('handlebars');

    var html = require('text!./user.template.html');
    var template = Handlebars.compile(html);

    return Backbone.View.extend({
        tagName: 'li',

        initialize: function () {
            _.bindAll(this, 'alertName');
            this.render();
        },
        events: {
            'click': 'alertName'
        },
        render: function () {
            this.$el.html(template({user: this.model.toJSON()}));
        },
        alertName: function () {
            alert(this.model.get('name'));
        }
    });
});
