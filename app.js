requirejs.config({
    baseUrl: 'app',
    paths: {
        'app': '../app',
        'jquery': '../bower_components/jquery/dist/jquery',
        'backbone': '../bower_components/backbone/backbone-min',
        'underscore': '../bower_components/underscore/underscore-min',
        'handlebars': '../bower_components/handlebars/handlebars.min',
        'text': '../bower_components/text/text'
    },

    shim: {
        'backbone': ['underscore']
    }
});

// Load the main app module to start the app
requirejs(['app/main']);